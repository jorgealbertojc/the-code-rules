# the-code-rules

Este repositorio tiene como único objetivo agregar reglamentación para los desarrolladores, estas reglas ayudarán a escribir un código mucho más legible, más fácil de entender y mucho más limpio. Esto ayudará también a que futuros desarrolladores que entren a los proyectos tengan una curva de aprendizaje más rápida ya que al saber como se debe codificar lograrán entender los procesos de códigos mucho más fácil.


## Estandarización `GIT`

* [Ramas y los formatos de nombres](git/ramas-y-formatos-de-nombres.md)
* [Commits y los formatos](git/commits-y-formatos.md)

## Estandarización `Javascript`

* [Espaciados, argumentos y parametros](code/js/espaciados-argumentos-y-parametros.md)
* [Declaración de variables](code/js/declaracion-de-variables.md)
* [Loops e iteraciones](code/js/loops-e-iteraciones.md)
* [Métodos y funciones](code/js/metodos-y-funciones.md)

## Estandarización `Go`

* [Herramientas y plugins](code/go/tools-and-plugins.md)
* [Organización de directorios](code/go/code-scaffolding.md)

> _la librería/binario `gofmt` y `goimports` hacen el trabajo de formatear el código y queda my compleensible así que para esto no hay reglas extra_

## Extras no documentados

* [TypeScript best practices](http://definitelytyped.org/guides/best-practices.html)

> _Se debe tener en cuenta que este repo está adaptado a los proyectos personales
o en los que participo._
