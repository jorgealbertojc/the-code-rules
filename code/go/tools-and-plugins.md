# Herramientas y plugins

Las herramientas son la base para poder llevar a cabo un trabajo, para esto tenemos un par de recomendaciones con respecto a los IDE's y lo que puedes usar dentro estos para llevar a cabo el trabajo encomendado.

## 1 .- Atom IDE

Atom es de los mejores editores que hemos usado, aún no es el mejor por que tiene problemas verdaderamente serios con los archivos que son demasiado largos pero hace bastante bien su trabajo.

Para instalarlo solo tienen que ir a la página oficial de [Atom](http://atom.io) y descargarse el binario que les acople con su SO.

Si lo deseas, puedes usar uno de nuestros snippets que te ayudarán a la intalación de atom y sus plugins
> _necesitas tener instalado `go`_

```bash
git clone https://gist.github.com/arkosnoemarenom/de1fc7e2e1094cafa60591e3c6a92a95 atom-installer
```

Una vez descargado se puede usar el siguiente comando:

```bash
sudo bash ./atom-installer/install-atom-and-plugins.sh --install-atom=true
# este paso lo tienes que hacer como root
```

## 2 .- Atom Plugins

Para instalar los plugins de atom usando el instalador que se menciona en el tema anterior puedes usar el siguiente comando:

```bash
bash ./atom-installer/install-atom-and-plugins.sh --install-atom-plugins=true
# para este caso no hacen falta los permisos de root
```

> _si instalas los plugins como root te pondrá todos los plugins en `/root/.atom/...` por lo cual si los quieres intalados en tu cuenta sólo usa el comando tal y como está_

## 3 .- Dependencias de `go-plus`/`go-debug`

Para este caso sólo es necesario abrir el atom ( _en su primer ejecución_ ) y permitir que `go-plus` y `go-debug` instalen todas las dependencias que necesitan. Finalmente nos pedirá que reiniciemos y listo quedará todo completamente instalado y listo para poder usarse.
