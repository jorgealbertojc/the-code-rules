# Scaffolding principal de un proyecto

Para que tu proyecto sea legible se deben colocar los directorios necesarios para poder colocar el código dentro de estos y que no se quede regado por todo el repositorio.

Para esto la propuesta de directorios es la siguiente:

## Directorios principales

Los directorios que se proponen como principales son los siguientes

```bash
.
├── bin
├── cli
│   └── cli_name
├── cmd
│   └── command_name
├── docs
├── hack
├── pkg
│   └── v0
│       └── service_name
├── tests
│   ├── e2e
│   ├── int
│   └── unit
└── vendor
```
### Definición por directorio
`bin`: Directorio destino para todos los binarios compilados. Este directorio no debe subirse al repositorio si no es absolutamente necesario.

`cli/...`: Este directorio está destinado a todos los clientes ( `command-line` ) que serán únicamente consumidores de una API, no se podrán comunicar directamente a los servicios ya que esto pondría en riesgo la integridad de cada servicio por lo cual si existe una API dentro del desarrollo un CLI será lo más conveniente para poder consumir esa API sin necesidad de tener que importar ningún servicio dentro de estos CLI.

Lo mejor para estos CLI es que se tengan configuraciones muy sencillas, ya sea en variables de entorno o en argumentos que se le pasen a este. Por ejemplo:

```bash
export PROJECT_KEY_ID=""
export PROJECT_DATABASE_USERNAME=""
export PROJECT_HOSTNAME=""
# etc.

go run ./cli/command/main.go --port 3128 --input @path/to/request-body.json
```

`cmd/...`: En este directorio se colocan todos los paquetes `main` de cada uno de los proyectos, por ejemplo si existe más de un proyecto dentro del mismo repositorio cada uno tendrá su propio `main.go` y este debe estar en este directorio
> _Los archivos `main.go` deben ser únicamente invocadores y no debe haber lógica dentro de ellos ( Esto se describe más adelante )_.

`docs`: Toda la documentación interna debe ir en este directorio a excepción del `README.md` principal que debe ir en la raíz del proyecto.

`hack`: En casi todos los proyectos se necesitan trucos especiales para poder ejecutar o construir o hasta documentar, este directorio es precisamente para todos los _hacks_ que suelen tener los proyectos. La organización de este directorio es completamente libre ya que depende completamente del proyecto.

`pkg/v${X}/...` Todos los servicios que ofrezca el proyecto deben estar versionados y dentro de su directorio que es representativo de su funcionalidad. En GO se escriben paquetes que, en comparativa, son una abstracción de una `Class` de java, cada paquete debe contener unicamente cosas de su funcionalidad y la funcionalidad extra que se llegue a necesitar dentro del proyecto se debe agregar como funcionalidad en otro paquete. En casos muy específicos se puede definir un directorio común que se encuentre a la altura de los directorios de versión, pero esta funcionalidad debe ser completa, absoluta y estrictamente común para todos los servicios.

`tests/...`: Los tests deben estar concentrados en un directorio específico, esto por que al colocarlos a la misma altura que el servicio el orden de los archivos puede verse afectada, además de que si para un servicio tenemos muchos archivos los tests pueden hacer que los archivos de funcionalidad se pierdan y se termine tardando mucho tiempo en corregir algo ya que los archivos no son visibles en un vistazo rápido. Los test unitarios deben estar en el mismo directorio y a la misma altura que el archivo de funcionalidad.

Los tests deben estar divididos por *tests de integración* y *tests end2end*.

`vendor/...` Este directorio se refiere únicamente a las dependencias externas al sistema que serán manejadas siempre por el software `glide` que es un manejador de dependencias. Este directorio no se sube al repositorio a menos que sea absolutamente necesario.

## Archivos principales

Los archivos principales son los siguientes ( _puede haber menos pero no puede haber más_ ):

```bash
.
├── Dockerfile          # archivo que ayuda a construir la imagen de docker para poder hacer tests o hacer funcionar el servicio en local
├── .dockerignore       # arcihvos y directorios que la docker ignorará al momento de construir
├── .editorconfig       # configuración del editor para que todo el código se formateé de la misma forma en cualquier editor
├── .gitignore          # archivos ignorados por git
├── glide.lock          # archivo lock que ayuda a mantener fijas las versiones de las librerías de go
├── glide.yaml          # configuración principal de glide
├── Jenkinsfile         # archivo que contiene la configuración del pipeline de jenkins
├── .key                # configuración de variables globales que sean requeridas por el proyecto
├── Makefile            # archivo que contiene los index que ayudan a la construcción o ejecución del proyecto
├── package.json        # configuración del proyecto y dependencias del proyecto
├── package-lock.json   # seguro que ayuda mantener estaticas las dependencias externas del proyecto
└── README.md           # inicio de la documentación del proyecto
```

## Vista general de un proyecto construido con GO

```bash
.
├── bin
├── cli
│   └── cli_name
├── cmd
│   └── command_name
├── docs
├── hack
├── local
├── pkg
│   └── v0
│       └── service_name
├── tests
│   ├── e2e
│   ├── int
│   └── unit
├── vendor
├── Dockerfile
├── .dockerignore
├── .editorconfig
├── .gitignore
├── glide.lock
├── glide.yaml
├── Jenkinsfile
├── .key
├── Makefile
├── package.json
├── package-lock.json
└── README.md
```
